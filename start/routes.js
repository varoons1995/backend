'use strict'
/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/
//Rest API 
const axios = require('axios')

/** @type {import('@adonisjs/framework/src/Env')} */
const Env = use('Env')

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Logger = use('Logger')
var Pusher = require('pusher');
const xlxs = require('xlsx');

//User Route
// Route.get('/users' , 'UserController.login')
// Route.post('/users', 'UserController.register' )
// Route.post('/users/forgot', 'UserController.forgot')
// Route.get('/users/:id','UserController.getPosts').middleware(['auth'])

Route.post('/deposit/kbank' , 'DepositController.KBank')
Route.post('/deposit/scb' , 'DepositController.scb')
Route.post('/deposit/krungsri' , 'DepositController.krungsri')
Route.post('/deposit/edit' , 'DepositController.updateTrasactionDepostiForAddCredit').middleware(['auth'])
Route.get('/deposit/pending' , 'DepositController.Transaction_Deposit_Pending').middleware(['auth'])
Route.post('/deposit/findbydate' , 'DepositController.findByDateBetweenAndStatus').middleware(['auth'])
Route.get('/deposit/all/:id' , 'DepositController.FindAllByUsername').middleware(['auth'])
Route.post('/deposit/trxstatus' , 'DepositController.TrxStatus').middleware(['auth'])

Route.get('/withdraw/all' ,'WithdrawController.FindAll').middleware(['auth'])
Route.post('/withdraw/scb' ,'WithdrawController.WithdrawSCB').middleware(['auth'])
Route.get('/withdraw/test','WithdrawController.CheckOTP').middleware(['auth'])
Route.post('/withdraw/credit' ,'WithdrawController.withdrawCredit').middleware(['auth'])
Route.post('/withdraw/trx' , 'WithdrawController.withdrawCreditTest').middleware(['auth'])
Route.post('/withdraw/queue' , 'WithdrawController.RedisQueueWithdraw').middleware(['auth'])
Route.get('/withdraw/all/:id' , 'WithdrawController.FindAllByUsername').middleware(['auth'])
Route.post('/withdraw/findBydate' , 'WithdrawController.resultWithdraw').middleware(['auth'])
Route.post('/withdraw/edit' , 'WithdrawController.updateTrasactionWithdraw').middleware(['auth'])

Route.get('/menu/all' , 'MenuController.FindAll').middleware(['auth'])
Route.post('/menu/addmenu' , 'MenuController.addMenu').middleware(['auth'])

Route.get('/menuclass/all' , 'MenuClassController.FindAll').middleware(['auth'])
Route.post('/menuclass/addmenuclass' , 'MenuClassController.addMenuClass').middleware(['auth'])

Route.get('/credit/all' , 'CreditController.FindAll').middleware(['auth'])
// Route.post('/credit/addcredit' , 'CreditController.addCredit' ).middleware(['auth'])
Route.post('/credit/addcreditapi','CreditController.addCreditAPI').middleware(['auth'])
Route.post('/credit/findbycreatedate' , 'CreditController.findByCreateDate').middleware(['auth'])
Route.post('/credit/sumCredit' , 'CreditController.sumCredit').middleware(['auth'])

Route.get('/bankowner/all' , 'BankOwnerController.FindAll').middleware(['auth'])
Route.post('/bankowner/add' , 'BankOwnerController.addBankOwner').middleware(['auth'])
Route.post('/bankowner/edit' , 'BankOwnerController.editBankOwner').middleware(['auth'])

Route.post('/staff/login' , 'StaffController.login')
Route.get('/staff/all' , 'StaffController.FindAll').middleware(['auth'])
Route.post('/staff/addstaff' , 'StaffController.addStaff').middleware(['auth'])
Route.post('/staff/edit' , 'StaffController.EditStaff').middleware(['auth'])

Route.get('/member/all' , 'MemberController.FindAll').middleware(['auth'])
Route.post('/member/addmember' , 'MemberController.addMember').middleware(['auth'])
Route.get('/member/:username', 'MemberController.findByUsername').middleware(['auth'])
Route.post('/member/agenttype' ,'MemberController.findByDateAndAgentType').middleware(['auth'])
Route.post('/member/edit' , 'MemberController.editMember').middleware(['auth'])
Route.post('/member/trxlist' , 'MemberController.trxlistMember').middleware(['auth'])

Route.get('/bank/all' , 'BankController.FindAll').middleware(['auth'])

Route.get('bankowner/all', 'BankOwnerController.FindAll').middleware(['auth'])
Route.post('bankowner/add' , 'BankOwnerController.addBankOwner').middleware(['auth'])
Route.post('bankowner/edit' , 'BankOwnerController.editBankOwner').middleware(['auth'])

Route.get('/channel/all' , 'ChannelController.FindAll').middleware(['auth'])

Route.get('/website/all' , 'WebsiteController.FindAll').middleware(['auth'])

Route.get('/staffclass/all' , 'StaffClassController.FindAll').middleware(['auth'])

// Route.get('/tttt' , 'DepositController.testWhere')

Route.post('/home' , 'HomeController.Reportall').middleware(['auth'])

// Route.post('/testdeposit' , 'DepositController.testDeposit')

// Route.post('/home/test' , 'HomeController.Reportall').middleware(['auth:api'])

// Route.post('test/selenium' , 'WithdrawController.testSelenium')

// Route.get('/', () => {
//   Logger
//   .transport('file')
//   .info(new Date().toLocaleString()+'Request URL With')
//   return { username: 'servertest' }
// })

// Wildcard Route
Route.any('*', () => {
  return 
})


Route.get('/addFileXlxs',() => {
  // Path File 
  var workbook = XLSX.readFile('Master.xlsx');
  var sheet_name_list = workbook.SheetNames;
  var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
  console.log(xlData);
  return {message : 'test'}
})



