'use strict'

const Database = use('Database')


class MenuClassController {

    async FindAll(){
        const query = Database.table('MENUCLASS')

        const MenuClassAll = await query
        .innerJoin('STAFFCLASS', 'STAFFCLASS.ID', 'MENUCLASS.STAFFCLASSID')
        
        console.log(MenuClassAll)
        return MenuClassAll;
    }

    async addMenuClass(request, response){
        const query = Database.table('MENUCLASS')
        
        const body = request._request_._original
        console.log(body)	
        // var today = new Date();  
        const addMenuClass = await query.insert({
            'STAFFCLASSID':body.STAFFCLASSID,
            'MENUID':body.MENUID
        })
        if(addMenuClass){
            response = addMenuClass[0]
        }else{
            response = 'Error'
        }
        
        return {message : response}
    }

}

module.exports = MenuClassController
