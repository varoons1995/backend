'use strict'

/** @type {import('@adonisjs/framework/src/Env')} */
const Env = use('Env')
const axios = require('axios')
const { Builder, By, Key, until , Options} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const Database = use('Database')
const Bull = use('Rocketseat/Bull')
const Job = use('App/Jobs/Withdraw')

class WithdrawController {

  async withdrawCredit(request , response){
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    var modalData = {
      memberID : '',
      bankOwner :'',
      trxStatus : '',
      balance : '',
      actionBy : '',
      remark : '',
      trxGear : ''
    }
    const body = request._request_._original;
    var ufausername = body.MEMBER_USERNAME
    var credit = body.CREDIT
    const query = Database.table('WEBSITE')
    let website = await query.where('ID' , body.WEBSITE_ID).first()
    
    var agenusername = website.USERNAME
    var agenpassword = website.PASSWORD
    console.log(ufausername , credit , agenusername , agenpassword)
          try{
            await axios({
              method: 'get',
              url: 'http://apibot.ufatool.com/bot',
              params : 
                { method :'requesttoken',
                  agenusername : agenusername,
                  ufausername : ufausername,
                  logmethod : 'delcredit'
                }
            })
              .then(response => {
                //console.log("response-test: ", response)
                
                var data_response = response;
                console.log(data_response.data)
       
                if(data_response.data.status == 'success'){
                  var token = data_response.data.token ;
                 axios({
                  method: 'get',
                  url: 'http://apibot.ufatool.com/bot',
                  params : 
                    { method :'delcredit',
                      agenusername : agenusername,
                      agenpassword : agenpassword,
                      ufausername : ufausername,
                      credit : credit,
                      token : token,
                    }
                })
                  .then(response => {
                    console.log("response: ", response.data)
                    if(response.data.status == 'success'){
                      modalData.memberID = body.ID
                      modalData.balance = body.CREDIT
                      modalData.trxStatus = 'pending'
                      modalData.actionBy = body.ACTION_BY
                      modalData.trxGear = 'auto'
                      modalData.date = dt
                    }else{
                      modalData.memberID = body.ID
                      modalData.balance = body.CREDIT
                      modalData.trxStatus = 'pending'
                      modalData.actionBy = body.ACTION_BY
                      modalData.trxGear = 'auto'
                      modalData.date = dt
                      modalData.remark = 'UFABET Api มีปัญหา กรุณาเติมมือ'
                      //let vv = await this.InsertTransaction_Withdraw(modalData)
                    }
                    // do something about response
                  })
                  .catch(err => {
                    console.error(err)
                  })
                }else{
                  console.log('ID Error')
                }
  
                // do something about response
              })
              .catch(err => {
                console.error(err)
              })
            } catch(err) {
              console.error(err)
            }
      }

    async CheckOTP(ref){
      const query = Database.table('OTP_RAW')
      console.log(ref)
      var reqOtp = 0 ;
      reqOtp = ref;
      var response
      const OTP = await query.where('REF_NUMBER',reqOtp)
      if(OTP.length > 0){
        response = OTP[0].OTP_NUMBER
      }
      else{
        response = 0
      }
      console.log(response)
      return response;
    }


    async FindAll(){
      const query = Database.table('TRANSACTION_WITHDAW')
      const trxWithdaw = await query.where('TRANSACTION_STATUS' ,'success')
      return trxWithdaw;
  }

  async FindAllByUsername(param){
    console.log(param)
    const query = Database.table('TRANSACTION_WITHDAW')
    const trxWithdaw = await query.where('TRANSACTION_STATUS' ,'success' ).where('MEMBER_ID',param.params.id)
    console.log(trxWithdaw)
    return trxWithdaw;
  }

  async InsertTransaction_Withdraw(request , response){
    const query = Database.table('TRANSACTION_WITHDAW')
    const body = request
    console.log(body)	
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    try{
      const InsertTransaction_Withdraw = await query
      .insert({
        "MEMBER_ID" : body.MEMBER_ID, 
        "BANK_OWNER_ID" : body.BANK_OWNER_ID, 
        "TRANSACTION_STATUS" : 'pending', 
        "BALANCE" : body.BALANCE, 
        "CREATEDATE" : dt, 
        "UPDATEDATE" : dt,
        "ACTION_BY":body.ACTION_BY,
        "UFABET_STATUS":"success"
      })
      if(InsertTransaction_Withdraw){
          response = 'Success'
      }else{
          response = 'Error'
      }            
      return {message : response}

    }catch(err){
        console.error(err)
    }
  }

  async InsertTransaction_WithdrawAPIFAIL(request , response){
    const query = Database.table('TRANSACTION_WITHDAW')
    const body = request
    console.log(body)	
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    try{
      const InsertTransaction_Withdraw = await query
      .insert({
        "MEMBER_ID" : body.MEMBER_ID, 
        "BANK_OWNER_ID" : body.BANK_OWNER_ID, 
        "TRANSACTION_STATUS" : 'failed', 
        "BALANCE" : body.BALANCE, 
        "CREATEDATE" : dt, 
        "UPDATEDATE" : dt,
        "ACTION_BY":body.ACTION_BY,
        "UFABET_STATUS":"failed"
      })
      if(InsertTransaction_Withdraw){
          response = 'Success'
      }else{
          response = 'Error'
      }            
      return {message : response}

    }catch(err){
        console.error(err)
    }
  }

  async RedisQueueWithdraw(request){
    //const body = request._request_._original;
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const body = request;
    const query = Database.table('BANK_OWNER')
    let g =  await query.where('BANK_TYPE','withdraw').where('BANK_STATUS','Y').first()
    console.log(g)
    let status
    let message
    const t = {
      MEMBER_ID : body.ID,
      BANK_OWNER_ID : g.ID,
      TRANSACTION_STATUS : '',
      BALANCE : body.AMOUNT,
      BANK_CREATEDATE : '',
      CREATEDATE : dt,
      SLIP :'',
      REMARK : '',
      TRANSACTION_GEAR : '',
      NUMBER :body.BANK_ACCOUNT_NUMBER,
      ACTION_BY:body.ACTIONBY,
      BANK_NAME:body.BANK_NAME,
      BANKOWNERDETAIL : g
    }

      if(g.BANK_GEAR == 'auto'){
        console.log(t)
        await Bull.add(Job.key, t)
        status =220
        message = 'add to queue'
      }else{
          let d = await this.InsertTransaction_Withdraw(t) 
          console.log(d)
          if(d.message == 'Success'){
            status = 200
            message = 'Add Trx Success'
          }else{
            status = 300
            message = 'Error DB'
          }
    }
    return {status:status,
            message : message}
  }

  async updateRedisQueueWithdraw(request){
    //const body = request._request_._original;
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const body = request;
    const query = Database.table('BANK_OWNER')
    let g =  await query.where('BANK_TYPE','withdraw').where('BANK_STATUS','Y').first()
    console.log('updateBody :'+body)
    let status
    let message
    const t = {
      MEMBER_ID : body.MEMBER_ID,
      BANK_OWNER_ID : g.ID,
      TRANSACTION_STATUS : '',
      BALANCE : body.AMOUNT,
      BANK_CREATEDATE : '',
      CREATEDATE : dt,
      SLIP :'',
      REMARK : '',
      TRANSACTION_GEAR : '',
      NUMBER :body.BANK_ACCOUNT_NUMBER,
      ACTION_BY:body.ACTIONBY,
      BANK_NAME:body.BANK_NAME,
      TRX_WITHDRAW_ID:body.TRX_WITHDRAW_ID,
      BANKOWNERDETAIL : g
    }

      if(g.BANK_GEAR == 'auto'){
        console.log(t)
        await Bull.add(Job.key, t)
        status =220
        message = 'add to queue'
      }else{
          if(d.message == 'Success'){
            status = 200
            message = 'Add Trx Success'
          }else{
            status = 300
            message = 'Error DB'
          }
    }
    return {status:status,
            message : message}
  }

  async withdrawCreditTest(request , response){
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const body = request._request_._original
    let sd = 'Message Backend'
    let as = '200'
    console.log(body)	
     let getMember = await member.where('ID',body.ID).first()
     console.log(getMember)
        const queryWeb = Database.table('WEBSITE')
        const updateApi = Database.table('TRANSACTION_WITHDAW')
        const webAgent = await queryWeb.where('ID',getMember.WEBSITE_ID).first();
        console.log(webAgent)
        try{
           let v = await axios({
              method: 'get',
              url: 'http://apibot.ufatool.com/bot',
              params : 
                { method :'requesttoken',
                  agenusername : webAgent.USERNAME,
                  ufausername : getMember.MEMBER_USERNAME,
                  logmethod : 'delcredit'
                }
            })
            if(v.data.status == 'error'){
              as = '301',
              sd = 'UFABET API ERROR'
            }else{
              console.log("response: ", v.data)
              var token = v.data.token ;    
              let t =  await  axios({
                  method: 'get',
                  url: 'http://apibot.ufatool.com/bot',
                  params : 
                    { method :'delcredit',
                      agenusername : webAgent.USERNAME,
                      agenpassword : webAgent.PASSWORD,
                      ufausername : getMember.MEMBER_USERNAME,
                      credit : body.AMOUNT,
                      token : token,
                    }
                })
                console.log(t.data)
                if(t.data.status == 'error_credit'){
                  console.log('error')
                  as = '200',
                  sd = 'ไม่สามารถถอนได้ ยอดเงินที่เหลือมี ' + t.data.balance
                  // await this.InsertTransaction_WithdrawAPIFAIL(body)
                }else if(t.data.status == 'success'){
                  as = '200'
                  sd = 'ทำรายการเสร็จ รออัปเดตจากทางระบบ'
                  await this.RedisQueueWithdraw(body)
                }
                else{
                  console.log('error')
                  as = '200',
                  sd = 'UFA API Error รอสักครู่แล้ว ทำรายการใหม่'
                  //await this.InsertTransaction_WithdrawAPIFAIL(body)
                }
              }
         
            } catch(err) {
              console.error(err)
            }

            return {status : as,
                    message : sd}
            
  }

  async updateTrasactionWithdraw(request, response){
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const queryTrx = Database.table('TRANSACTION_WITHDAW')
    const body = request._request_._original
    let v
    if(body.ACTION == "Hand"){
      v = await queryTrx.where('ID' , body.TRX_WITHDRAW_ID)
              .update({
                "TRANSACTION_STATUS" : 'success',
                "ACTION_BY" : body.ACTIONBY,
                "TRANSACTION_GEAR":'manual',
                "UPDATEDATE": dt
              })
    }else if (body.ACTION == "Reset"){
      console.log(body) 
        if(body.UFABET_STATUS == "success" && body.TRANSACTION_STATUS != "reject"){
        v = await queryTrx.where('ID' , body.TRX_WITHDRAW_ID)
          .update({
            "TRANSACTION_STATUS" : 'pending',
            "ACTION_BY" : body.ACTIONBY,
            "TRANSACTION_GEAR":'auto',
            "UPDATEDATE": dt
          })
        let vt = await this.updateRedisQueueWithdraw(body)
        }else{
          console.log('start update queue')
          v = await queryTrx.where('ID' , body.TRX_WITHDRAW_ID)
          .update({
            "TRANSACTION_STATUS" : 'pending',
            "ACTION_BY" : body.ACTIONBY,
            "TRANSACTION_GEAR":'auto',
            "UPDATEDATE": dt
          })
        let vt = await this.updateWithdrawCreditTest(body)
        }
    }else if(body.ACTION == "Reject"){
      v = await queryTrx.where('ID' , body.TRX_WITHDRAW_ID)
      .update({
        "TRANSACTION_STATUS" : 'pending',
        "ACTION_BY" : body.ACTIONBY,
        "TRANSACTION_GEAR":'auto',
        "UPDATEDATE": dt
      })
       let vt = await this.rejectCredit(body)
    }
    
   return v 
  }

  async updateWithdrawCreditTest(request){
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const body = request
    const ufaapi = Database.table('TRANSACTION_WITHDAW')
    let sd = 'Message Backend'
    let as = '200'
    console.log('update withdraw :'+body)	
     let getMember = await member.where('ID',body.MEMBER_ID).first()
     console.log(getMember)
        const queryWeb = Database.table('WEBSITE')
        const webAgent = await queryWeb.where('ID',getMember.WEBSITE_ID).first();
        console.log(webAgent)
        try{
           let v = await axios({
              method: 'get',
              url: 'http://apibot.ufatool.com/bot',
              params : 
                { method :'requesttoken',
                  agenusername : webAgent.USERNAME,
                  ufausername : getMember.MEMBER_USERNAME,
                  logmethod : 'delcredit'
                }
            })
            if(v.data.status == 'error'){
              as = '301',
              sd = 'UFABET API ERROR'
            }else{
              console.log("response: ", v.data)
              var token = v.data.token ;    
              let t =  await  axios({
                  method: 'get',
                  url: 'http://apibot.ufatool.com/bot',
                  params : 
                    { method :'delcredit',
                      agenusername : webAgent.USERNAME,
                      agenpassword : webAgent.PASSWORD,
                      ufausername : getMember.MEMBER_USERNAME,
                      credit : body.AMOUNT,
                      token : token,
                    }
                })
                console.log(t.data)
                if(t.data.status == 'error'){
                  console.log('error')
                  as = '301',
                  sd = 'UFABET API ERROR'
                }else{
                  as = '200'
                  sd = 'Del Credit in UFABET Success'
                  await ufaapi.where('ID', body.TRX_WITHDRAW_ID).update({
                    "UFABET_STATUS" : 'success'
                  })
                  await this.updateRedisQueueWithdraw(body)
                }
              }
         
            } catch(err) {
              console.error(err)
            }

            return {status : as,
                    message : sd}
  }

  async rejectCredit(request , response){
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const body = request
    const ufaapi = Database.table('TRANSACTION_WITHDAW')
    let sd = 'Message Backend'
    let as = '200'
    console.log('update withdraw :'+body)	
     let getMember = await member.where('ID',body.MEMBER_ID).first()
     console.log(getMember)
        const queryWeb = Database.table('WEBSITE')
        const webAgent = await queryWeb.where('ID',getMember.WEBSITE_ID).first();
        console.log(webAgent)
        try{
           let v = await axios({
              method: 'get',
              url: 'http://apibot.ufatool.com/bot',
              params : 
                { method :'requesttoken',
                  agenusername : webAgent.USERNAME,
                  ufausername : getMember.MEMBER_USERNAME,
                  logmethod : 'addcredit'
                }
            })
            if(v.data.status == 'error'){
              as = '301',
              sd = 'UFABET API ERROR'
            }else{
              console.log("response: ", v.data)
              var token = v.data.token ;    
              let t =  await  axios({
                  method: 'get',
                  url: 'http://apibot.ufatool.com/bot',
                  params : 
                    { method :'addcredit',
                      agenusername : webAgent.USERNAME,
                      agenpassword : webAgent.PASSWORD,
                      ufausername : getMember.MEMBER_USERNAME,
                      credit : body.AMOUNT,
                      token : token,
                    }
                })
                console.log(t.data)
                if(t.data.status == 'error'){
                  console.log('error')
                  as = '301',
                  sd = 'UFABET API ERROR'
                }else{
                  as = '200'
                  sd = 'Reject Credit in UFABET Success'
                  await ufaapi.where('ID', body.TRX_WITHDRAW_ID).update({
                    "UFABET_STATUS" : 'success',
                    "TRANSACTION_STATUS":'reject'
                  })
                }
              }
         
            } catch(err) {
              console.error(err)
            }

            return {status : as,
                    message : sd}
            
      }

      async resultWithdraw(request , response){
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        const body = request._request_._original
        let tr_fdate = body.CREATEDATE
        let tr_edate = body.ENDDATE
        let query
        let countquery 
        let status
        let message1
        let message2
        if(body.USERNAME !=''){
          query = await Database.raw("SELECT TW.ID AS TRANSACTION_WITHDAW_ID, B.BANKCODE,B.BANKNAME,M.ID ,M.BANK_ACCOUNT_NUMBER ,M.BANK_ACCOUNT_NAME,M.MEMBER_USERNAME ,TW.BALANCE, TW.BANK_CREATEDATE,TW.ACTION_STATUS ,TW.CREATEDATE,TW.UPDATEDATE,TW.BANK_BALANCE ,TW.ACTION_BY, TW.REMARK, TW.SLIP ,TW.TRANSACTION_STATUS ,TW.TRANSACTION_GEAR, TW.UFABET_STATUS FROM UFABET.TRANSACTION_WITHDAW TW LEFT JOIN UFABET.MEMBER M ON TW.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK B ON B.ID =  M.BANK_ID LEFT JOIN UFABET.BANK_OWNER BO ON TW.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TW.TRANSACTION_STATUS NOT IN ('hiding') AND TW.UPDATEDATE BETWEEN ? AND ? AND M.MEMBER_USERNAME = ? ORDER BY TW.UPDATEDATE DESC",[tr_fdate,tr_edate ,body.USERNAME]);
          countquery = await Database.raw("SELECT BO.BANK_TITLE, BO.BANK_ACCOUNT_NUMBER, COUNT(TW.BANK_OWNER_ID) AS COUNT_TW,SUM(TW.BALANCE) AS TOTAL_BALANCE,TW.TRANSACTION_GEAR, W.WEBSITENAME FROM UFABET.TRANSACTION_WITHDAW TW LEFT JOIN UFABET.MEMBER M ON TW.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER BO ON TW.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND M.MEMBER_USERNAME = ? GROUP BY BO.BANK_ACCOUNT_NUMBER",[tr_fdate,tr_edate ,body.USERNAME])
          status = 200
          message1 = query[0]
          message2 = countquery[0]
        }else{
          if(body.AGENTTYPE != ''){
            query = await Database.raw("SELECT TW.ID AS TRANSACTION_WITHDAW_ID, B.BANKCODE ,B.BANKNAME ,M.ID ,M.BANK_ACCOUNT_NUMBER ,M.BANK_ACCOUNT_NAME,M.MEMBER_USERNAME ,TW.BALANCE, TW.BANK_CREATEDATE, TW.ACTION_STATUS,TW.CREATEDATE,TW.UPDATEDATE,TW.BANK_BALANCE ,TW.ACTION_BY, TW.REMARK, TW.SLIP ,TW.TRANSACTION_STATUS ,TW.TRANSACTION_GEAR, TW.UFABET_STATUS FROM UFABET.TRANSACTION_WITHDAW TW LEFT JOIN UFABET.MEMBER M ON TW.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK B ON B.ID =  M.BANK_ID LEFT JOIN UFABET.BANK_OWNER BO ON TW.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TW.TRANSACTION_STATUS NOT IN ('hiding') AND TW.UPDATEDATE BETWEEN ? AND ?AND W.WEBSITENAME = ? ORDER BY TW.UPDATEDATE DESC",[tr_fdate,tr_edate ,body.AGENTTYPE]);  
            countquery = await Database.raw("SELECT BO.BANK_TITLE, BO.BANK_ACCOUNT_NUMBER, COUNT(TW.BANK_OWNER_ID) AS COUNT_TW,SUM(TW.BALANCE) AS TOTAL_BALANCE,TW.TRANSACTION_GEAR, W.WEBSITENAME FROM UFABET.TRANSACTION_WITHDAW TW LEFT JOIN UFABET.MEMBER M ON TW.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER BO ON TW.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND W.WEBSITENAME = ? GROUP BY BO.BANK_ACCOUNT_NUMBER",[tr_fdate,tr_edate ,body.AGENTTYPE])
            status = 200
            message1 = query[0]
            message2 = countquery[0]
          }else{
            query = await Database.raw("SELECT TW.ID AS TRANSACTION_WITHDAW_ID, B.BANKCODE,B.BANKNAME ,M.ID,M.BANK_ACCOUNT_NUMBER ,M.BANK_ACCOUNT_NAME,M.MEMBER_USERNAME ,TW.BALANCE, TW.BANK_CREATEDATE,  TW.ACTION_STATUS , TW.CREATEDATE,TW.UPDATEDATE,TW.BANK_BALANCE ,TW.ACTION_BY, TW.REMARK, TW.SLIP ,TW.TRANSACTION_STATUS ,TW.TRANSACTION_GEAR, TW.UFABET_STATUS FROM UFABET.TRANSACTION_WITHDAW TW LEFT JOIN UFABET.MEMBER M ON TW.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK B ON B.ID =  M.BANK_ID LEFT JOIN UFABET.BANK_OWNER BO ON TW.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TW.TRANSACTION_STATUS NOT IN ('hiding') AND TW.UPDATEDATE BETWEEN ? AND ? ORDER BY TW.UPDATEDATE DESC",[tr_fdate,tr_edate]);
            countquery = await Database.raw("SELECT BO.BANK_TITLE, BO.BANK_ACCOUNT_NUMBER, COUNT(TW.BANK_OWNER_ID) AS COUNT_TW,SUM(TW.BALANCE) AS TOTAL_BALANCE,TW.TRANSACTION_GEAR, W.WEBSITENAME FROM UFABET.TRANSACTION_WITHDAW TW LEFT JOIN UFABET.MEMBER M ON TW.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER BO ON TW.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? GROUP BY BO.BANK_ACCOUNT_NUMBER",[tr_fdate,tr_edate])
            status = 200
            message1 = query[0]
            message2 = countquery[0]
          }
        }
        
        return {
          status : status,
          trxList:message1,
          bankCount : message2
        }
      }

  // async testSelenium(){
    
  //     console.log('Check Selenium');
  //     // let options = new ChromeOptions()
  //     // options.addArguments("--no-sandbox");
  //     // options.addArguments("--disable-dev-shm-usage");
  //     // let driver = new ChromeDriver(options);
  //     // chrome_options.add_argument('--headless')
  //     // chrome_options.add_argument('--no-sandbox')
  //     // chrome_options.add_argument('--disable-dev-shm-usage')

  //     let options   = new chrome.Options();
  //     options.addArguments('--headless'); // note: without dashes
  //     options.addArguments('--no-sandbox');
  //     options.addArguments('--disable-dev-shm-usage')
  //     let driver = await new Builder().forBrowser('chrome').setChromeOptions(options).build();
  //     console.log(driver)
  //     try {
  //           await driver.get('https://m.scbeasy.com/online/easynet/mobile/login.aspx');
  //           await driver.findElement(By.name('tbUsername')).sendKeys('paramat4252');
  //           await driver.findElement(By.name('tbPassword')).sendKeys('4252Paramat');
  //           await new Promise(resolve => setTimeout(resolve, 500));
  //           await driver.findElement(By.id('Login_Button')).click()
  //           await new Promise(resolve => setTimeout(resolve, 1500));
  //           const result_complete = await driver.findElement(By.id('content')).getText();
  //           console.log(result_complete)
            
  //     }finally{
  //        await driver.quit();
  //     }
  // }

  async testSelenium(request , message){
    const query = Database.table('TRANSACTION_WITHDAW')
    const body = request._request_._original
    const dataMessage = 'success'
    console.log("Insert :"+body)	
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    let response 
    try{
      const InsertTransaction_Withdraw = await query
      .insert({
        "MEMBER_ID" : body.MEMBER_ID, 
        "BANK_OWNER_ID" : body.BANK_OWNER_ID, 
        "TRANSACTION_STATUS" : dataMessage.status, 
        "BALANCE" : body.BALANCE, 
        "BANK_CREATEDATE":dt,
        "CREATEDATE" : dt, 
        "UPDATEDATE" : dt,
        "ACTION_BY":body.ACTION_BY,
        "SLIP":dataMessage.message,
        "REMARK":body.REMARK,
        "TRANSACTION_GEAR":'auto',
        "UFABET_STATUS":'success',
        "BANK_BALANCE":dataMessage.balance,
      })
      if(InsertTransaction_Withdraw > 0){
          response = 'Success'
      }else{
          response = 'Error'
      }            
      return {message : response}

    }catch(err){
        console.error(err)
    }
  }

}

module.exports = WithdrawController