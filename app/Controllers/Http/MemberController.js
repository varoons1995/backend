'use strict'

const Database = use('Database')
const axios = require('axios')


class MemberController {

    async FindAll(){
        const query = Database.table('MEMBER')

        const MemberAll = await query
        .innerJoin('BANK', 'BANK.ID', 'MEMBER.BANK_ID')
        .innerJoin('WEBSITE', 'WEBSITE.ID', 'MEMBER.WEBSITE_ID')
        .innerJoin('CHANEL', 'CHANEL.ID', 'MEMBER.CHANELID')

        console.log(MemberAll)
        return MemberAll;
    }
    async addMember(request, response){
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        const query = Database.table('MEMBER')
        const body = request._request_._original
        let username 
        let password 
        let sd = 'Message Backend'
        let as = '200'
        // const check_account_number = body.BANK_ACCOUNT_NUMBER
        // console.log(check_account_number.length);
        let username_accountnumber = await query.where('BANK_ACCOUNT_NUMBER', body.BANK_ACCOUNT_NUMBER)
        .orWhere('MEMBER_USERNAME', body.MEMBER_USERNAME).first()
        console.log(username_accountnumber)
        if(username_accountnumber){  
                  as = '300',
                  sd = 'Username or BankAccount Already !!!'
        }else{
            const queryWeb = Database.table('WEBSITE')
            const webAgent = await queryWeb.where('ID',body.WEBSITE_ID).first();
            console.log(webAgent)
            await axios({
                method: 'get',
                url: 'http://apibot.ufatool.com/bot',
                params : 
                    { 
                      method :'addmember',
                      agenusername : webAgent.USERNAME,
                      agenpassword : webAgent.PASSWORD,
                      ufausername : body.MEMBER_USERNAME,
                      ufapassword : webAgent.PASSWORD_USER,
                      textcontact : 'API'
                    }
                }).then(response => {
                    console.log(response.data)
                    if(response.data.status == 'error'){
                      as = '301',
                      sd = 'UFABET API ERROR'
                    }else{
                      as = '200'
                      sd = 'Add Member in UFABET Success'
                      username = response.data.ufa_username
                      password = response.data.ufa_password
                    }
                  })
                  .catch(err => {
                    console.error(err)
                  })
                  if(as == 200){
                    console.log('Add Member to DB')
                    try{
                      const addMember = await query.insert({
                          'MEMBER_USERNAME':username,
                          'MEMBERFIRSTNAME':body.MEMBERFIRSTNAME,
                          'MEMBERLASTNAME':body.MEMBERLASTNAME,
                          'PHONE':body.PHONE,
                          'BANK_ID':body.BANK_ID,
                          'BANK_ACCOUNT_NUMBER':body.BANK_ACCOUNT_NUMBER,
                          'BANK_ACCOUNT_NAME':body.BANK_ACCOUNT_NAME,
                          'WEBSITE_ID':body.WEBSITE_ID,
                          'CHANELID':body.CHANELID,
                          'CREDITE':body.CREDITE,
                          'CREATEDATE':dt,
                          'LASTUPDATE':dt
                        })
                            if(addMember){
                            console.log(addMember)
                              const tt =  await queryWeb.where('ID',body.WEBSITE_ID).update('WEBSITECODE',body.WEBSITECODE)                            
                              const queryReturn = Database.table('MEMBER')
                              return {status : as ,
                                      username :username ,
                                      password :password }
                            }else{  
                              return {status : as,
                                      message : sd };
                            }
                           }catch(e){
                            console.error(e)
                          }
                  }
        }
    }

    async findByUsername(params){
      let param = params._request_.params
      // const query = Database.table('MEMBER')
      // let member = await query.where('MEMBER_USERNAME', param.username)
      // .first();
      console.log(param)
      const query = await Database.raw('SELECT M.ID, M.MEMBER_USERNAME, M.MEMBERFIRSTNAME, M.MEMBERLASTNAME, M.PHONE, M.BANK_ID, B.BANKNAME, M.BANK_ACCOUNT_NUMBER, M.BANK_ACCOUNT_NAME, M.WEBSITE_ID,W.WEBSITENAME, M.CHANELID, C.CHANELNAME, M.CREDITE, M.CREATEDATE, M.LASTUPDATE FROM UFABET.MEMBER M INNER JOIN UFABET.BANK B ON B.ID =  M.BANK_ID INNER JOIN UFABET.WEBSITE W ON W.ID = M.WEBSITE_ID INNER JOIN UFABET.CHANEL C ON C.ID = M.CHANELID WHERE M.MEMBER_USERNAME = ?', [param.username])
      if(query[0][0]){
        return query[0][0]
      }else{
        return 
      }
    }

    async findByDateAndAgentType(request , response){
      const body = request._request_._original
      // const query = Database.table('MEMBER')
      // With Agent Type
      let fd = body.CREATEDATE
      let ld = body.ENDDATE
      let status
      let member
      if(body.USERNAME != ''){
        member = await Database.raw("SELECT M.ID AS MEMBER_ID,M.MEMBER_USERNAME,M.PHONE,M.BANK_ACCOUNT_NUMBER,M.MEMBERFIRSTNAME,M.MEMBERLASTNAME,CONCAT(M.MEMBERFIRSTNAME,' ', M.MEMBERLASTNAME) AS MEMBER_FULLNAME,B.ID,B.BANKCODE,B.BANKNAME,M.CREATEDATE,W.WEBSITENAME,C.CHANELNAME FROM UFABET.MEMBER M LEFT JOIN UFABET.BANK B ON B.ID =  M.BANK_ID LEFT JOIN UFABET.WEBSITE W ON W.ID =  M.WEBSITE_ID LEFT JOIN UFABET.CHANEL C ON C.ID =  M.CHANELID WHERE M.MEMBER_USERNAME = ?",[body.USERNAME])
        member = member[0]
        status = 200
      }else {
          if(body.AGENTTYPE != ''){
            console.log(body.AGENTTYPE)
            member = await Database.raw("SELECT M.ID AS MEMBER_ID,M.MEMBER_USERNAME,M.PHONE,M.BANK_ACCOUNT_NUMBER,M.MEMBERFIRSTNAME,M.MEMBERLASTNAME,CONCAT(M.MEMBERFIRSTNAME, ' ', M.MEMBERLASTNAME) AS MEMBER_FULLNAME,B.ID,B.BANKCODE,B.BANKNAME,M.CREATEDATE,W.WEBSITENAME,C.CHANELNAME FROM UFABET.MEMBER M LEFT JOIN UFABET.BANK B ON B.ID =  M.BANK_ID LEFT JOIN UFABET.WEBSITE W ON W.ID =  M.WEBSITE_ID LEFT JOIN UFABET.CHANEL C ON C.ID =  M.CHANELID WHERE M.CREATEDATE BETWEEN  ? AND ? AND W.WEBSITENAME = ? ORDER By M.CREATEDATE desc",[fd , ld ,body.AGENTTYPE ])
            member = member[0]
            status = 200
          }else{
            member = await Database.raw("SELECT M.ID AS MEMBER_ID,M.MEMBER_USERNAME,M.PHONE,M.BANK_ACCOUNT_NUMBER,M.MEMBERFIRSTNAME,M.MEMBERLASTNAME,CONCAT(M.MEMBERFIRSTNAME, ' ', M.MEMBERLASTNAME) AS MEMBER_FULLNAME,B.ID,B.BANKCODE,B.BANKNAME,M.CREATEDATE,W.WEBSITENAME,C.CHANELNAME FROM UFABET.MEMBER M LEFT JOIN UFABET.BANK B ON B.ID =  M.BANK_ID LEFT JOIN UFABET.WEBSITE W ON W.ID =  M.WEBSITE_ID LEFT JOIN UFABET.CHANEL C ON C.ID =  M.CHANELID WHERE M.CREATEDATE BETWEEN  ? AND ? ORDER By M.CREATEDATE desc",[fd , ld ])
            member = member[0]
            status = 200
          }
      }
      

      return {status : status,
              message: member}
    }


    async editMember(request,response){
      var dt = new Date();
      dt.setHours(dt.getHours()+7);
      const query = Database.table('MEMBER')
      const body = request._request_._original;
      console.log(body)
      const EditMember = await query.where('ID',body.ID)
      .update({
          'MEMBER_USERNAME':body.MEMBER_USERNAME,
          'MEMBERFIRSTNAME':body.MEMBERFIRSTNAME,
          'MEMBERLASTNAME':body.MEMBERLASTNAME,
          'PHONE':body.PHONE,
          'BANK_ID':body.BANK_ID,
          'BANK_ACCOUNT_NUMBER':body.BANK_ACCOUNT_NUMBER,
          'BANK_ACCOUNT_NAME':body.BANK_ACCOUNT_NAME,
          'LASTUPDATE':dt
      })
      console.log(EditMember)
      if(EditMember > 0){
          response = 'Suscess'
      }else{
          response = 'Error'
      }
  
      return {message : response}
  }

  async trxlistMember(request , response){
    const body = request._request_._original;
    let fd = body.CREATEDATE
    let ld = body.ENDDATE
    let status 
    let member
    if(body.USERNAME != ''){
      member = await Database.raw("SELECT M.ID, M.MEMBER_USERNAME ,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID) AS SUM_COUNT_TD, IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TD, (SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID) AS SUM_COUNT_TW, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TW, (IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) - IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND TW.MEMBER_ID = M.ID),0)) AS SUM_NET FROM UFABET.MEMBER M LEFT JOIN WEBSITE W ON M.WEBSITE_ID = W.ID WHERE  ((SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.MEMBER_ID = M.ID AND TD.UPDATEDATE BETWEEN ? AND ?) > 0) AND M.MEMBER_USERNAME = ?" ,[fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd,ld,body.USERNAME])
      member = member[0]
      status = 200
    }else {
        if(body.AGENTTYPE != ''){
          console.log(body.AGENTTYPE)
          member = await Database.raw("SELECT M.ID, M.MEMBER_USERNAME ,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID) AS SUM_COUNT_TD, IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TD, (SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID) AS SUM_COUNT_TW, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TW, (IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) - IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND TW.MEMBER_ID = M.ID),0)) AS SUM_NET FROM UFABET.MEMBER M LEFT JOIN WEBSITE W ON M.WEBSITE_ID = W.ID WHERE  ((SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.MEMBER_ID = M.ID AND TD.UPDATEDATE BETWEEN ? AND ?) > 0) AND W.WEBSITENAME = ?" ,[fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd ,ld,body.AGENTTYPE])
          member = member[0]
          status = 200
        }else{
          member = await Database.raw("SELECT M.ID, M.MEMBER_USERNAME ,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID) AS SUM_COUNT_TD, IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TD, (SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID) AS SUM_COUNT_TW, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TW, (IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) - IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND TW.MEMBER_ID = M.ID),0)) AS SUM_NET FROM UFABET.MEMBER M LEFT JOIN WEBSITE W ON M.WEBSITE_ID = W.ID WHERE  ((SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.MEMBER_ID = M.ID AND TD.UPDATEDATE BETWEEN ? AND ?) > 0)" ,[fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld,fd,ld,])
          member = member[0]
          status = 200
        }
    }

    return {
      status : status,
      message: member
    }

  }

}

module.exports = MemberController

