'use strict'

//Rest API 
const axios = require('axios')

/** @type {import('@adonisjs/framework/src/Env')} */
const Env = use('Env')

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Logger = use('Logger')
const Pusher = require('pusher');
const Database = use('Database')

class DepositController {

    async  KBank(request, response){
     var dt = new Date();
     dt.setHours(dt.getHours()+7);
     const member = Database.table('MEMBER')
     const body = request._request_._original
     Logger
        .transport('file')
        .info(dt.toLocaleString()+' SMS Message  '+body.data.sms)
        const raw_sms = Database.table('RAW_SMS')
        let smsRaw = await raw_sms.where('RAW_SMS',body.data.sms)
        if(smsRaw.length == 0){
        let insertDBRawSms = await raw_sms.insert({RAW_SMS : body.data.sms})
        var modalData = {
          sms_type: '',
          sms_bank_number: '',
          sms_from_number: '',
          sms_price: '',
          sms_date :'',
          sms_remark :''
        }
        // Pre Data
        var smsContent = body.data.sms;
        var rpSms1 = smsContent.replace('รับโอนจาก', 'รับโอนจาก ');
        var smsContent = rpSms1.replace('คงเหลือ', 'คงเหลือ ');
        var ar_replace = ['บช','บ.','บ','X'];
        //Split SMS Data 

        var splitSms = smsContent.split(" ");
        console.log(splitSms)
        // ขาฝาก Normal
        if(splitSms.length == 8 || splitSms.length == 10 ||splitSms.length == 12 ){

            if(splitSms[3] == 'รับโอนจาก'){
            var typeSms = 'Deposit'
            modalData.sms_type = typeSms;
            modalData.sms_balance = splitSms[7].replace(/บช|บ.|บ|X/gi,''); 
            modalData.sms_bank_number = splitSms[2].replace(/บช|บ.|บ|X/gi,''); 
            modalData.sms_from_number = splitSms[4].replace(/บช|บ.|บ|X/gi,''); 
            modalData.sms_price = splitSms[5].replace(/บช|บ.|บ|X/gi,''); 
            modalData.sms_date = splitSms[0]+' '+splitSms[1]
           // modalDate.sms_date = splitSms[0]
            console.log(modalData)
            var account_number = '%'+modalData.sms_from_number+'%'
            const query = await Database.raw('SELECT M.ID, M.MEMBER_USERNAME, M.MEMBERFIRSTNAME, M.MEMBERLASTNAME, M.PHONE, M.BANK_ID, B.BANKNAME, M.BANK_ACCOUNT_NUMBER, M.BANK_ACCOUNT_NAME, M.WEBSITE_ID, W.WEBSITENAME, W.USERNAME AS WEB_USERNAME, W.PASSWORD AS WEB_PASSWORD, M.CHANELID, C.CHANELNAME, M.CREDITE, M.CREATEDATE, M.LASTUPDATE FROM UFABET.MEMBER M INNER JOIN UFABET.BANK B ON B.ID =  M.BANK_ID INNER JOIN UFABET.WEBSITE W ON W.ID =  M.WEBSITE_ID INNER JOIN UFABET.CHANEL C ON C.ID =  M.CHANELID WHERE M.BANK_ACCOUNT_NUMBER LIKE ? ' , [account_number])
            if(query[0][0]){
              // insert table with member id
            let vv = await this.addCredit(query[0][0].WEB_USERNAME , query[0][0].WEB_PASSWORD , query[0][0].MEMBER_USERNAME , modalData.sms_price ,query[0][0])
            console.log(vv.status)
             if(vv.message == 'success'){
             // insert table db 
             let gg = await this.addTrxDepositHaveMemberID(query[0][0], modalData , vv.message)
             console.log(gg)
                 if(gg[0]){
                  this.pusherTrigger();
                   return {message :'success'}
                 }else{
                   return {error :'update DB error'}
                 }
             }else{
               // insert table db with status api ufa error
              let gg = await this.addTrxDepositHaveMemberID(query[0][0], modalData , 'pending')
              modal.remark = 'API UFA Error'
              if(gg[0]){
                this.pusherTrigger();
                return {message :'success'}
              }else{
                return {error :'update DB error'}
              }
             }
           }else{
             console.log('Find Not Found')
             // insert table without member id
             let gg = await this.addTrxDepositNotMemberID(modalData , 'pending') 
             this.pusherTrigger();
           }
          }else if(splitSms[3].includes('เงินเข้า') == true){
                     
            var typeSms = 'Deposit'
            modalData.sms_type = typeSms;
            modalData.sms_balance = splitSms[5].replace(/บช|บ.|บ|X/gi,''); 
            modalData.sms_bank_number =splitSms[2].replace(/บช|บ.|บ|X/gi,''); 
            modalData.sms_from_number ='null'
            modalData.sms_price = splitSms[3].replace(/เงินเข้า|บช|บ.|บ|X/gi,''); 
            modalData.sms_date = splitSms[0]+' '+splitSms[1]
            // Query User
            //console.log('test'+ modalData) 
            let gg = await this.addTrxDepositNotMemberID(modalData , 'pending') 
            if(gg[0]){
              this.pusherTrigger();
              return {message : 'succes'}
            }else{
              return {message : 'error'}
            }

            }else{
            return { status : 'Error',
                    mesagge : smsContent}
            } 
            // Query User  
         }else{
            return { status : 'Error',
                     mesagge : smsContent}
         } 
     
        return {status : 'Success'}
      }else{
        console.log('Duplicate SMS Message :'+ body.data.sms)
        return 
      }

    }

    async scb(request, response){
        const body = request._request_._original
        console.log(body)
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        const raw_sms = Database.table('RAW_SMS')
        let smsRaw = await raw_sms.where('RAW_SMS',body.data.sms)
        if(smsRaw.length == 0){
        let insertDBRawSms = await raw_sms.insert({RAW_SMS : body.data.sms})
        var modalData = {
          sms_type: '',
          sms_bank_number: '',
          sms_from_number: '',
          sms_price: '',
          sms_date :'',
          sms_remark :''
        }
        const member = Database.table('MEMBER')
        Logger
        .transport('file')
        .info(dt.toLocaleString()+' SMS Message  '+body.data.sms)
        // Pre Data
        var smsContent = body.data.sms;
        var rawData = body.data.sms;
        console.log(smsContent)
        var rpSms1 = smsContent.replace('/x',' ');
        smsContent = rpSms1.replace('เข้าx', ' '); 
        smsContent = smsContent.replace('@',' ');
        smsContent = smsContent.replace('จาก','');
        //Split SMS Data 
        var splitSms = smsContent.split(" ");
        var bankDate = splitSms[0]+' '+splitSms[1]
        console.log(bankDate)
        console.log(splitSms)
        if(splitSms.length == 10 ){
            if(splitSms[4] == '000000'){
             var smsType = 'Deposit-CDM'
             modalData.sms_type = smsType;
             modalData.sms_bank_number = splitSms[5]; 
             modalData.sms_from_number = splitSms[4]; 
             modalData.sms_price = splitSms[2]; 
             modalData.sms_date = bankDate;
             console.log(modalData)  
             let gg = await this.addTrxDepositNotMemberID(modalData , 'pending')
             if(gg[0]){
              this.pusherTrigger();
              return {message : 'success'}
             }else{
              return {message : 'error'}
             }
            }else{
              if(splitSms[4].length == 6){
            var smsType = 'Deposit-ATM'
            
             // Query User
             modalData.sms_type = smsType;
             modalData.sms_bank_number = splitSms[5]; 
             modalData.sms_from_number = splitSms[4]; 
             modalData.sms_price = splitSms[2]; 
             modalData.sms_date = bankDate;
             console.log(modalData)
              //
              var account_number = '%'+modalData.sms_from_number+'%'
              const query = await Database.raw('SELECT M.ID, M.MEMBER_USERNAME, M.MEMBERFIRSTNAME, M.MEMBERLASTNAME, M.PHONE, M.BANK_ID, B.BANKNAME, M.BANK_ACCOUNT_NUMBER, M.BANK_ACCOUNT_NAME, M.WEBSITE_ID, W.WEBSITENAME, W.USERNAME AS WEB_USERNAME, W.PASSWORD AS WEB_PASSWORD, M.CHANELID, C.CHANELNAME, M.CREDITE, M.CREATEDATE, M.LASTUPDATE FROM UFABET.MEMBER M INNER JOIN UFABET.BANK B ON B.ID =  M.BANK_ID INNER JOIN UFABET.WEBSITE W ON W.ID =  M.WEBSITE_ID INNER JOIN UFABET.CHANEL C ON C.ID =  M.CHANELID WHERE M.BANK_ACCOUNT_NUMBER LIKE ?' , [account_number])
              if(query[0][0]){
                // insert table with member id
              let vv = await this.addCredit(query[0][0].WEB_USERNAME , query[0][0].WEB_PASSWORD , query[0][0].MEMBER_USERNAME , modalData.sms_price ,query[0][0])
              console.log(vv.status)
               if(vv.message == 'success'){
               // insert table db 
               let gg = await this.addTrxDepositHaveMemberID(query[0][0], modalData , vv.message)
               console.log(gg)
                   if(gg[0]){
                    this.pusherTrigger();
                     return {message :'success'}
                   }else{
                     return {error :'update DB error'}
                   }
               }else{
                 // insert table db with status api ufa error
                let gg = await this.addTrxDepositHaveMemberID(query[0][0], modalData , 'pending')
                modal.remark = 'API UFA Error'
                if(gg[0]){
                  this.pusherTrigger();
                  return {message :'success'}
                }else{
                  return {error :'update DB error'}
                }
               }
             }else{
               console.log('Find Not Found')
               // insert table without member id
               let gg = await this.addTrxDepositNotMemberID(modalData , 'pending') 
               this.pusherTrigger();
             }
            }
        }
           // 
        }else if (splitSms.length == 9){
            var smsType = 'Deposit SMS With SCB'
            // Query User
            var sms_type = smsType;
            var sms_bank_number = splitSms[4]; 
            var sms_from_number = ' '; 
            var sms_price = splitSms[2]; 
            console.log(sms_type,sms_bank_number,sms_from_number,sms_price)
            modalData.sms_type = smsType;
            modalData.sms_bank_number = sms_bank_number; 
            modalData.sms_from_number = sms_from_number; 
            modalData.sms_price = splitSms[2]; 
            modalData.sms_date = bankDate;
            let vv = await this.addTrxDepositNotMemberID(modalData , 'pending')
            if(vv[0]){
              this.pusherTrigger();
              return {message : 'success'}
            }else{
              return {message :'error'}
            }
        }else if (splitSms.length == 16 ){
            const OTP_RAW = Database.table('OTP_RAW')
            console.log(splitSms[15])
            var smsUserAccount = splitSms[2].replace(/<|>/gi,''); 
            var otp = splitSms[5].replace(/<|>|Ref./gi,''); 
            var refOtp = splitSms[6].replace(/<|>/gi,''); 
            console.log(smsUserAccount,otp,refOtp)
            //Insert DB
            var inserDB = await OTP_RAW.insert({REF_NUMBER: refOtp,OTP_NUMBER : otp})
            console.log(inserDB)
            if(inserDB > 0 ){
                    this.pusherTrigger();
                    response =  {status : 'success',
                    message : inserDB[0]
                }
            }else{
                    response =  { status : 'Error',
                    message : 'Error Insert to Table'
                }
            }
        }
        else if(splitSms.length == 13){
          const OTP_RAW = Database.table('OTP_RAW')
          console.log(splitSms)
           var otp = splitSms[3].replace(/<|>|Ref./gi,''); 
           var refOtp = splitSms[4].replace(/<|>|ใน/gi,''); 
           console.log(smsUserAccount,otp,refOtp)
          //Insert DB
          var inserDB = await OTP_RAW.insert({REF_NUMBER: refOtp,OTP_NUMBER : otp})
          console.log(inserDB)
          if(inserDB > 0 ){
                  this.pusherTrigger();
                  response =  {status : 'success',
                  message : inserDB[0]
              }
          }else{
                  response =  { status : 'Error',
                  message : 'Error Insert to Table'
              }
          }
        }
        else if(splitSms.length == 14){
          const OTP_RAW = Database.table('OTP_RAW')
          console.log(splitSms)
           var otp = splitSms[4].replace(/<|>|Ref./gi,''); 
           var refOtp = splitSms[5].replace(/<|>|ใน/gi,''); 
           console.log(smsUserAccount,otp,refOtp)
          //Insert DB
          var inserDB = await OTP_RAW.insert({REF_NUMBER: refOtp,OTP_NUMBER : otp})
          console.log(inserDB)
          if(inserDB > 0 ){
                  this.pusherTrigger();
                  response =  {status : 'success',
                  message : inserDB[0]
              }
          }else{
                  response =  { status : 'Error',
                  message : 'Error Insert to Table'
              }
          }
        }
        else {
            response =  {status : 'Error',
                        message : rawData
            }
        }
        return response
      }else{
        console.log('Duplicate SMS Message :'+ body.data.sms)
        return 
      }
    }
    
    async krungsri(request, response){
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        const body = request._request_._original
        const raw_sms = Database.table('RAW_SMS')
        let smsRaw = await raw_sms.where('RAW_SMS',body.data.sms)
        if(smsRaw.length == 0){
        let insertDBRawSms = await raw_sms.insert({RAW_SMS : body.data.sms})
        const member = Database.table('MEMBER')
        var modalData = {
          sms_type: '',
          sms_bank_number: '',
          sms_from_number: '',
          sms_price: '',
          sms_date :'',
          sms_remark :''
        }
        Logger
        .transport('file')
        .info(dt.toLocaleString()+' SMS Message  '+body.data.sms)
        var smsContent = body.data.sms;
        var rawData = body.data.sms;
        console.log(smsContent)
        smsContent = smsContent.replace('xxx','');
        smsContent = smsContent.replace('x','');
        var splitSms = smsContent.split(" ");
        console.log(splitSms)
        if (splitSms.length == 11){
            var smsType = 'Deposit';
            // Query User 
            modalData.sms_type = smsType;
            modalData.sms_bank_number = splitSms[1]; 
            modalData.sms_from_number = ' '; 
            modalData.sms_price = splitSms[3]; 
            modalData.sms_date = splitSms[6].replace(',', ' ')
            modalData.sms_date =modalData.sms_date.replace('(','')
            modalData.sms_date =modalData.sms_date.replace(')','')
            console.log(modalData.sms_date)
            let vv = await this.addTrxDepositNotMemberID(modalData , 'pending')
            if(vv[0]){
              this.pusherTrigger();
              return {message : 'success'}
            }else{
              return {message :'error'}
            }
        }else{
            return {status :'Error',
                    message : rawData
                   }
        }
      }else{
        console.log('Duplicate SMS Message :'+ body.data.sms)
        return 
      }
    }




    async addCredit(agu , agp , ufan , credit , body){
        console.log(
          'add credit' + agu + agp + ufan + credit
        )
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        credit = credit.replace(',','')
        credit = credit.replace(',','')
        console.log(body.MEMBER_USERNAME)
        var agenusername = agu
        var agenpassword = agp
        var ufausername = ufan
        var credit = credit
        let  status
        let v =   await axios({
                  method: 'get',
                  url: 'http://apibot.ufatool.com/bot',
                  params : 
                    { method :'requesttoken',
                      agenusername : agenusername,
                      ufausername : ufausername,
                      logmethod : 'addcredit'
                    }
                  })
                  console.log(v.data)
               
         if(v.data.status == 'success') {
         
           let t = await axios({
                  method: 'get',
                  url: 'http://apibot.ufatool.com/bot',
                  params : 
                    { method :'addcredit',
                      agenusername : agenusername,
                      agenpassword : agenpassword,
                      ufausername : ufausername,
                      credit : credit,
                      token : v.data.token,
                    }
                })

                console.log(t.data)

                if(t.data.status == 'success'){
                  return {message : 'success'}
                }else{
                  return {message : 'error'}
                }
         }else{
           return {message : 'error'}
         } 
       }     
         
    async addTrxDepositHaveMemberID(body , modal ,status){
      // 
      var dt = new Date();
      dt.setHours(dt.getHours()+7);
      console.log(body)
      console.log(modal)
      // Find Bank Owner
      let account_number = '%'+modal.sms_bank_number+'%'
      let bo = await Database
      .from('BANK_OWNER')
      .whereRaw('BANK_ACCOUNT_NUMBER LIKE ? AND BANK_TYPE = "deposit"', [account_number]).first();
      // Insert Database
      console.log(bo)
      let id = body.ID
      let bankOwner = bo.ID
      let trxStatus = status
      let balance = modal.sms_price
      let actionby = ''
      let bankCrate = modal.sms_date
      let remark = modal.sms_remark
      let trxGear = 'auto'
      let trxDeposit = await Database.table('TRANSACTION_DEPOSIT')
      .insert({
        'MEMBER_ID':id,
        'BANK_OWNER_ID':bankOwner,
        'TRANSACTION_STATUS':trxStatus,
        'BALANCE':balance,
        'ACTION_BY':actionby,
        'BANK_CREATEDATE':bankCrate,
        'CREATEDATE':dt,
        'UPDATEDATE':dt,
        'REMARK':remark,
        'TRANSACTION_GEAR':trxGear
      })
 	console.log(trxDeposit)
      return trxDeposit;
    }

    async addTrxDepositNotMemberID(modal ,status){
      // 
      var dt = new Date();
      dt.setHours(dt.getHours()+7);
      console.log(modal)
      // Find Bank Owner
      let account_number = '%'+modal.sms_bank_number+'%'
      let bo = await Database
      .from('BANK_OWNER')
      .whereRaw('BANK_ACCOUNT_NUMBER LIKE ? AND BANK_TYPE = "deposit"', [account_number]).first();
      // Insert Database
      let id = ''
      let bankOwner = bo.ID
      let trxStatus = status
      let balance = modal.sms_price
      let actionby = ''
      let bankCrate = modal.sms_date
      let remark = modal.sms_remark
      let trxGear = 'auto'
      let trxDeposit = await Database.table('TRANSACTION_DEPOSIT')
      .insert({
        'MEMBER_ID':id,
        'BANK_OWNER_ID':bankOwner,
        'TRANSACTION_STATUS':trxStatus,
        'BALANCE':balance,
        'ACTION_BY':actionby,
        'BANK_CREATEDATE':bankCrate,
        'CREATEDATE':dt,
        'UPDATEDATE':dt,
        'REMARK':remark,
        'TRANSACTION_GEAR':trxGear
      })
      console.log(trxDeposit)
      return trxDeposit;
    }

    async updateTrasactionDepostiForAddCredit(request, response){
      var dt = new Date();
      dt.setHours(dt.getHours()+7);
      const queryTrx = Database.table('TRANSACTION_DEPOSIT')
      const body = request._request_._original
      console.log(body) 
            if(body.ACTION == 'Add'){
              let id =  body.MEMBER_USERNAME
              const query = await Database.raw('SELECT M.ID, M.MEMBER_USERNAME, M.MEMBERFIRSTNAME, M.MEMBERLASTNAME, M.PHONE, M.BANK_ID, B.BANKNAME, M.BANK_ACCOUNT_NUMBER, M.BANK_ACCOUNT_NAME, M.WEBSITE_ID, W.WEBSITENAME, W.USERNAME AS WEB_USERNAME, W.PASSWORD AS WEB_PASSWORD, M.CHANELID, C.CHANELNAME, M.CREDITE, M.CREATEDATE, M.LASTUPDATE FROM UFABET.MEMBER M INNER JOIN UFABET.BANK B ON B.ID =  M.BANK_ID INNER JOIN UFABET.WEBSITE W ON W.ID =  M.WEBSITE_ID INNER JOIN UFABET.CHANEL C ON C.ID =  M.CHANELID WHERE M.MEMBER_USERNAME = ?' , [id])
              let vv = await this.addCredit(query[0][0].WEB_USERNAME , query[0][0].WEB_PASSWORD , query[0][0].MEMBER_USERNAME , body.BALANCE ,query[0][0])
              if(vv.message == 'success'){
                try{
                  const editTransaction_Deposit = await queryTrx.where('ID',body.TRANSACTION_DEPOSIT_ID)
                  .update({
                    "MEMBER_ID":query[0][0].ID,
                    "TRANSACTION_GEAR":'manual',
                    "TRANSACTION_STATUS" : 'success', 
                    "ACTION_BY":body.ACTION_BY,
                    "REMARK" : body.REMARK,
                    "UPDATEDATE":dt
                  })
                  if(editTransaction_Deposit){
                    this.pusherTrigger();
                    return {
                      status : 200,
                      message : 'Success'            
                    }
                  }else{
                    return {
                      status : 300,
                      message : 'Error'    
                    }
                  }            
                }catch(err){
                    console.error(err)
                }
              }else{
                return {status : 300,
                        message :'API Error'}
              }
            
            }else if(body.ACTION == 'Hide'){
              try{
                const editTransaction_Deposit = await queryTrx.where('ID',body.TRANSACTION_DEPOSIT_ID)
                .update({
                  "TRANSACTION_STATUS" : 'hiding',
                  "ACTION_BY":body.ACTION_BY, 
                  "REMARK" : body.REMARK,
                  "UPDATEDATE":dt
                })
                if(editTransaction_Deposit){
                  this.pusherTrigger();
                  return {
                    status : 200,
                    message : 'Success'            
                  }
                }else{
                  return {
                    status : 300,
                    message : 'Error'    
                  }
                }            
        
              }catch(err){
                  console.error(err)
              }
          }else if(body.ACTION == 'AddManual'){
            try{
              const editTransaction_Deposit = await queryTrx.where('ID',body.TRANSACTION_DEPOSIT_ID)
              .update({
                "TRANSACTION_STATUS" : 'success',
                "ACTION_BY": body.ACTION_BY,
                "REMARK" : body.REMARK ,
                "TRANSACTION_GEAR":'manual',
                "UPDATEDATE":dt
              })
              if(editTransaction_Deposit){
                return {
                  status : 200,
                  message : 'Success'            
                }
              }else{
                return {
                  status : 300,
                  message : 'Error'    
                }
              }           
            }catch(err){
                console.error(err)
            }
            
          }
          
    }

    async Transaction_Deposit_Pending(){
      const query = Database.table('TRANSACTION_DEPOSIT')
      const Transaction_Deposit_Pending = await query
      .where('TRANSACTION_DEPOSIT.TRANSACTION_STATUS','pending')
      console.log(Transaction_Deposit_Pending)
      return Transaction_Deposit_Pending;
    }

    async TrxStatus(request , response){
      const body = request._request_._original
      let tr_status = body.TRANSACTION_STATUS;
      let tr_fdate = body.CREATEDATE;
      let tr_edate = body.ENDDATE;
      console.log(tr_status , tr_fdate , tr_edate)
      const query = await Database.raw("SELECT TD.ID ,TD.MEMBER_ID,B.BANK_TITLE,TD.CREATEDATE,TD.UPDATEDATE,TD.REMARK,TD.BALANCE,TD.ACTION_BY,TD.BANK_CREATEDATE,TD.TRANSACTION_GEAR FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.BANK_OWNER B ON TD.BANK_OWNER_ID = B.ID WHERE TD.TRANSACTION_STATUS = ? AND TD.UPDATEDATE BETWEEN ? AND ? ORDER BY TD.UPDATEDATE DESC",[tr_status,tr_fdate,tr_edate])
      let message = query[0]
      console.log(query)
      let status = 200
      return {status:status,
              message:message}
    }

    async Transaction_Deposit_Pending(){
      const query = Database.table('TRANSACTION_DEPOSIT')
      const Transaction_Deposit_Pending = await query
      .where('TRANSACTION_DEPOSIT.TRANSACTION_STATUS','pending')
      console.log(Transaction_Deposit_Pending)
      return Transaction_Deposit_Pending;
    }

    async TrxStatus(request , response){
      const body = request._request_._original
      let tr_status = body.TRANSACTION_STATUS;
      let tr_fdate = body.CREATEDATE;
      let tr_edate = body.ENDDATE;
      console.log(body)
      const query = await Database.raw("SELECT TD.ID ,TD.MEMBER_ID,B.BANK_TITLE,TD.CREATEDATE,TD.UPDATEDATE,TD.REMARK,TD.BALANCE,TD.ACTION_BY,TD.BANK_CREATEDATE,TD.TRANSACTION_GEAR FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.BANK_OWNER B ON TD.BANK_OWNER_ID = B.ID WHERE TD.TRANSACTION_STATUS = ? AND TD.UPDATEDATE BETWEEN ? AND ? ORDER BY TD.UPDATEDATE DESC",[tr_status,tr_fdate,tr_edate])
      let message = query[0]
      let status = 200
      return {status:status,
              message:message}
    }

    async findByDateBetweenAndStatus(request , response){
      const body = request._request_._original
      let tr_status = body.TRANSACTION_STATUS;
      let tr_fdate = body.CREATEDATE;
      let tr_edate = body.ENDDATE;
      var query;
      console.log(body)
      let sumTrx 
      let sumBankOwner
      let status
      let listTrx
      if(body.USERNAME != ''){
        sumTrx = await Database.raw("SELECT (SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? AND TD.TRANSACTION_GEAR = 'auto' AND M.MEMBER_USERNAME = ?) AS TRANSACTION_GEAR_AUTO,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? AND TD.TRANSACTION_GEAR = 'manual' AND M.MEMBER_USERNAME = ?) AS TRANSACTION_GEAR_MANUAL FROM DUAL",[tr_fdate ,tr_edate,body.USERNAME, tr_fdate ,tr_edate,body.USERNAME])
        sumTrx = sumTrx[0]
        sumBankOwner = await Database.raw("SELECT BO.BANK_TITLE, BO.BANK_ACCOUNT_NUMBER, COUNT(TD.BANK_OWNER_ID) AS COUNT_TD ,SUM(TD.BALANCE) AS TOTAL_BALANCE, TD.TRANSACTION_GEAR, W.WEBSITENAME FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER BO ON TD.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? AND M.MEMBER_USERNAME = ? GROUP BY BO.BANK_ACCOUNT_NUMBER;" , [tr_fdate,tr_edate ,body.USERNAME])
        sumBankOwner = sumBankOwner[0]
        listTrx = await Database.raw("SELECT TD.ID, BO.BANK_TITLE, M.MEMBER_USERNAME, TD.BANK_CREATEDATE, TD.CREATEDATE, TD.BALANCE, TD.ACTION_BY, TD.REMARK, TD.TRANSACTION_GEAR,W.WEBSITENAME FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER BO ON TD.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? AND M.MEMBER_USERNAME = ? ORDER BY TD.UPDATEDATE DESC " ,[tr_fdate,tr_edate,body.USERNAME])
        listTrx = listTrx[0]
        status=200
      }else{
          if(body.AGENTTYPE != ''){
            sumTrx = await Database.raw("SELECT (SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? AND TD.TRANSACTION_GEAR = 'auto' AND W.WEBSITENAME = ?) AS TRANSACTION_GEAR_AUTO,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? AND TD.TRANSACTION_GEAR = 'manual' AND W.WEBSITENAME = ?) AS TRANSACTION_GEAR_MANUAL FROM DUAL",[tr_fdate ,tr_edate,body.AGENTTYPE, tr_fdate ,tr_edate,body.AGENTTYPE])
            sumTrx = sumTrx[0]
            sumBankOwner = await Database.raw("SELECT BO.BANK_TITLE, BO.BANK_ACCOUNT_NUMBER,COUNT(TD.BANK_OWNER_ID) AS COUNT_TD ,SUM(TD.BALANCE) AS TOTAL_BALANCE, TD.TRANSACTION_GEAR, W.WEBSITENAME FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER BO ON TD.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? AND W.WEBSITENAME = ? GROUP BY BO.BANK_ACCOUNT_NUMBER;" , [tr_fdate,tr_edate ,body.AGENTTYPE])
            sumBankOwner = sumBankOwner[0]
            listTrx = await Database.raw("SELECT TD.ID, BO.BANK_TITLE, M.MEMBER_USERNAME, TD.BANK_CREATEDATE, TD.CREATEDATE, TD.BALANCE, TD.ACTION_BY, TD.REMARK, TD.TRANSACTION_GEAR,W.WEBSITENAME FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER BO ON TD.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? AND W.WEBSITENAME = ? ORDER BY TD.UPDATEDATE DESC" ,[tr_fdate,tr_edate,body.AGENTTYPE])
            listTrx = listTrx[0]
            status=200
          }else{
            sumTrx = await Database.raw("SELECT (SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? AND TD.TRANSACTION_GEAR = 'auto') AS TRANSACTION_GEAR_AUTO,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? AND TD.TRANSACTION_GEAR = 'manual') AS TRANSACTION_GEAR_MANUAL FROM DUAL",[tr_fdate ,tr_edate, tr_fdate ,tr_edate])
            sumTrx = sumTrx[0]
            sumBankOwner = await Database.raw("SELECT BO.BANK_TITLE, BO.BANK_ACCOUNT_NUMBER, COUNT(TD.BANK_OWNER_ID) AS COUNT_TD , SUM(TD.BALANCE) AS TOTAL_BALANCE, TD.TRANSACTION_GEAR, W.WEBSITENAME FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER BO ON TD.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? GROUP BY BO.BANK_ACCOUNT_NUMBER;" , [tr_fdate,tr_edate ])
            sumBankOwner = sumBankOwner[0]
            listTrx = await Database.raw("SELECT TD.ID, BO.BANK_TITLE, M.MEMBER_USERNAME, TD.BANK_CREATEDATE, TD.CREATEDATE, TD.BALANCE, TD.ACTION_BY, TD.REMARK, TD.TRANSACTION_GEAR,W.WEBSITENAME FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER BO ON TD.BANK_OWNER_ID = BO.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? ORDER BY TD.UPDATEDATE DESC" ,[tr_fdate,tr_edate])
            listTrx = listTrx[0]
            status=200
          }
      }
      
      return {
        status :status,
        sumtrx :sumTrx,
        sumBankOwner :sumBankOwner,
        listTrx : listTrx
      }
  }

  async FindAllByUsername(param){
    const query = Database.table('TRANSACTION_DEPOSIT')
    const trxWithdaw = await query.where('TRANSACTION_STATUS' ,'success' ).where('MEMBER_ID',param.params.id)
    console.log(trxWithdaw)
    return trxWithdaw;
  }

  async pusherTrigger(){
    var pusher = new Pusher({
      appId: Env.get('PUSHER_APP_ID', ''),
      key: Env.get('PUSHER_APP_KEY', ''),
      secret: Env.get('PUSHER_APP_SECRET', ''),
      cluster: Env.get('PUSHER_APP_CLUSTER', ''),
      encrypted: true
    });
    pusher.trigger('my-channel', 'my-event', {
      'message': 'refresh'
    });
  }

  async testDeposit(request, response){
    const body = request._request_._original
    const raw_sms = Database.table('RAW_SMS')
    let smsRaw = await raw_sms.where('RAW_SMS',body.data.sms)
        if(smsRaw.length == 0){
        let insertDBRawSms = await raw_sms.insert({RAW_SMS : body.data.sms})
        }else{
          console.log('Duplicate SMS Message :'+ body.data.sms)
          return 
        }
  }

}

module.exports = DepositController
