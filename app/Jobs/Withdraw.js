'use strict'

/** @type {import('@adonisjs/framework/src/Env')} */
const Env = use('Env')
const axios = require('axios')
const { Builder, By, Key, until , Options} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const Database = use('Database')
const Bull = use('Rocketseat/Bull')


class Withdraw {

  static get key() {
    return "withdraw-key";
  }

  async handle(job) {
    const { data } = job; 
    let t = await this.WithdrawSCB(data)
    console.log('Queue Result :'+ t.status)
    let v 
    if(data.TRX_WITHDRAW_ID){
      // Update DB
      console.log('if')
      v = await this.updateTransaction_Withdraw(data , t)
    }else{
      // Insert DB
     console.log(data.MEMBER_ID)
     v = await this.InsertTransaction_Withdraw(data , t)
  
    }
    console.log(v)
    return v;
  }

        async WithdrawSCB (modal) {
	        console.log('Check Selenium');
          console.log(modal)
          let options   = new chrome.Options();
          options.addArguments('--headless'); // note: without dashes
          options.addArguments('--no-sandbox');
          options.addArguments('--disable-dev-shm-usage')
          let driver = await new Builder().forBrowser('chrome').setChromeOptions(options).build();
          const bank_us = modal.BANKOWNERDETAIL.BANK_USERNAME;
          const bank_pw = modal.BANKOWNERDETAIL.BANK_PASSWORD;
          let status = 'failed'
          let message = ''
          let balance = ''
        
          try {
              // Login 
            await driver.get('https://m.scbeasy.com/online/easynet/mobile/login.aspx');
            await driver.findElement(By.name('tbUsername')).sendKeys(bank_us);
            await driver.findElement(By.name('tbPassword')).sendKeys(bank_pw);
            await new Promise(resolve => setTimeout(resolve, 500));
            await driver.findElement(By.id('Login_Button')).click()
              // Create TRX in DB
            await new Promise(resolve => setTimeout(resolve, 1500));
            await driver.findElement(By.id('DataProcess_lbTransfer')).click();
            await new Promise(resolve => setTimeout(resolve, 2500));
            const detailStep0 = await driver.findElement(By.id('content')).getText();
            console.log(detailStep0)
        
            // SCB To SCB 

            if(modal.BANK_NAME == 'ไทยพานิชย์'){
            console.log(modal)
            await driver.findElement(By.id('DataProcess_lbAnotherSCB')).click();
            await new Promise(resolve => setTimeout(resolve, 1500));
            await driver.findElement(By.id('DataProcess_lbAnotherAcc')).click();
            await new Promise(resolve => setTimeout(resolve, 2500));
            await driver.findElement(By.name('ctl00$DataProcess$tbToAcc')).sendKeys(modal.NUMBER);
            await driver.findElement(By.name('ctl00$DataProcess$tbAmount')).sendKeys(modal.BALANCE);
            await driver.findElement(By.name('ctl00$DataProcess$ddlMobile')).sendKeys("XX");
            await new Promise(resolve => setTimeout(resolve, 2500));
            await driver.findElement(By.name('ctl00$DataProcess$btNext')).click();
            const detailStep1 = await driver.findElement(By.id('content')).getText();
            console.log(detailStep1)

            }else{ // SCB To Anotherbank
              await driver.findElement(By.id('DataProcess_lbAnotherBank')).click();
              await new Promise(resolve => setTimeout(resolve, 2000));
              await driver.findElement(By.id('DataProcess_lbNoProfile')).click();
              await new Promise(resolve => setTimeout(resolve, 2500));
              await driver.findElement(By.name('ctl00$DataProcess$ddlBank')).sendKeys(modal.BANK_NAME);
              await driver.findElement(By.name('ctl00$DataProcess$tbToAcc')).sendKeys(modal.NUMBER);
              await driver.findElement(By.name('ctl00$DataProcess$ddlMobile')).sendKeys("XX");
              await driver.findElement(By.name('ctl00$DataProcess$tbAmt')).sendKeys(modal.BALANCE);
              await new Promise(resolve => setTimeout(resolve, 2500));
              await driver.findElement(By.name('ctl00$DataProcess$btNext')).click();
              const detailStep1 = await driver.findElement(By.id('content')).getText();
              console.log(detailStep1)
              
            }
            const detailStep2 = await driver.findElement(By.id('content')).getText();
            console.log(detailStep2)
            var ref_number = await driver.findElement(By.id('DataProcess_lbOTPRefNo')).getText();
            await new Promise(resolve => setTimeout(resolve, 35000));
            var otpNumber = 0 ;
            otpNumber = await this.CheckOTP(ref_number);

          if(otpNumber == 0){
          }else{
            console.log(otpNumber)
            await driver.findElement(By.name('ctl00$DataProcess$tbOTP')).sendKeys(otpNumber);
            await new Promise(resolve => setTimeout(resolve, 2500));
            await driver.findElement(By.name('ctl00$DataProcess$btConfirm')).click();
            await new Promise(resolve => setTimeout(resolve, 3500));
            const result_complete = await driver.findElement(By.id('content')).getText();
            console.log(result_complete)
            var t
           
                var checkStatus = result_complete.match(/ทำรายการสำเร็จ/g);
                console.log(checkStatus)
                if(checkStatus){
                     let res = result_complete.split(" ");
                     t = res[3].replace('\n','')
                     t = t.replace('เพื่อเข้าบัญชี' , '')
                    //  t = t.replace('กสิกรไทย' , '')
                     t = t.trim();
                     status = 'success'
                     message = result_complete
                     balance = t
                }
                
          }
            await driver.manage().deleteAllCookies();
          } 
          finally {
            await driver.quit();
            return {status :status,
                    message :message,
                    balance : t }
          }
        }

        async CheckOTP(ref){
        const query = Database.table('OTP_RAW')
        console.log(ref)
        var reqOtp = 0 ;
        reqOtp = ref;
        var response
        const OTP = await query.where('REF_NUMBER',reqOtp)
          if(OTP.length > 0){
            response = OTP[0].OTP_NUMBER
          }
          else{
            response = 0
          }
          console.log(response)
          return response;
        } 

        async InsertTransaction_Withdraw(data , message){
          const query = Database.table('TRANSACTION_WITHDAW')
          const body = data
          const dataMessage = message
          console.log("Insert :"+body.MEMBER_ID)	
          var dt = new Date();
          dt.setHours(dt.getHours()+7);
          let response 
          try{
            const InsertTransaction_Withdraw = await query
            .insert({
              "MEMBER_ID" : body.MEMBER_ID, 
              "BANK_OWNER_ID" : body.BANK_OWNER_ID, 
              "TRANSACTION_STATUS" : dataMessage.status, 
              "BALANCE" : body.BALANCE, 
              "BANK_CREATEDATE":dt,
              "CREATEDATE" : dt,
              "UPDATEDATE" : dt,
              "ACTION_BY":body.ACTION_BY,
              "SLIP":dataMessage.message,
              "REMARK":body.REMARK,
              "TRANSACTION_GEAR":'auto',
              "UFABET_STATUS":'success',
              "BANK_BALANCE":dataMessage.balance,
            })
            console.log(InsertTransaction_Withdraw)
            if(InsertTransaction_Withdraw > 0){
                response = 'Success'
            }else{
                response = 'Error'
            }            
            return {message : response}
      
          }catch(err){
              console.error(err)
          }
        }

        async updateTransaction_Withdraw(data , message){
          const query = Database.table('TRANSACTION_WITHDAW')
          const body = data
          const dataMessage = message
          console.log("Update :"+body)	
          var dt = new Date();
          dt.setHours(dt.getHours()+7);
          let response 
          try{
            const InsertTransaction_Withdraw = await query.where('ID',body.TRX_WITHDRAW_ID)
            .update({
              "TRANSACTION_STATUS" : dataMessage.status, 
              "ACTION_BY":body.ACTION_BY,
              "SLIP":dataMessage.message,
              "UPDATEDATE" : dt,
              "BANK_BALANCE":dataMessage.balance,
              "REMARK":body.REMARK,
            })
            if(InsertTransaction_Withdraw){
                response = 'Success'
            }else{
                response = 'Error'
            }            
            return {message : response}
      
          }catch(err){
              console.error(err)
          }

        }


}

module.exports = Withdraw;
