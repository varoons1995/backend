'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Staff extends Model {

    static get table () {
        return 'STAFF'
    }

    static get primaryKey () {
        /* \return 'page_id' and 'language'; */
        return 'ID'
    }

    tokens() {
        return this.hasMany("App/Models/Token")
    }

    
}

module.exports = Staff
